#include <stdio.h>
#include <time.h>
#include <ctype.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

//CONSTANTES
#define ESCENA_W 32
#define ESCENA_H 20
#define PARED_SIM '#'
#define SUELO_SIM '.'
#define PJ_VIDA 100
#define PJ_MOVIMIENTO 5
#define PJ_SIMBOLO '@'
#define CONSOLA_LEN 50
#define CONSOLA_MENSAJES 80/TAM_FUENTE
#define CONSOLA_X 25
#define CONSOLA_Y 400

//SDL CONSTANTES
#define PANTALLA_W 640
#define PANTALLA_H 480
#define PATH 100
#define TTFTEXT 100
#define PERSONAJE_COL 1
#define TILESET_PATH "Assets/Tileset.png"
#define TILES 20
#define TAM_FUENTE 12

#define SUELO_Y 0
#define SUELO_X 0

#define PARED_Y 20
#define PARED_X 0
void ponerTexto(char texto[TTFTEXT], int x, int y, int r, int g, int b, struct stSDL * SDLApp);


struct stPj{
	int x, y;
	char simbolo;
	float vida,vidaMax, movimiento,movimientoMax;
};
struct stConsola{
	int indiceMensajes;
	char mensajes[CONSOLA_MENSAJES][CONSOLA_LEN];
	void escribirConsola(char mensaje[CONSOLA_LEN])
	{
		int w;
		if(indiceMensajes<CONSOLA_MENSAJES)
		{
			sprintf(mensajes[indiceMensajes],"%s",mensaje);
			indiceMensajes++;
		}
		else
		{
			for(w = 0; w < CONSOLA_MENSAJES; w++)
			{	
				if(w < CONSOLA_MENSAJES-1)
				{
				       	sprintf(mensajes[w],"%s",mensajes[w+1]);
				}
				else
				{
					sprintf(mensajes[w],"%s",mensaje);
				}
			}

		}	
	}
	void dibujarConsola(struct stSDL * SDLApp)
	{
		char txt[TTFTEXT];
		int w = 0;
		for ( w = 0; w < indiceMensajes; w++)
		{
			sprintf(txt,"%s",mensajes[w]);
			ponerTexto(txt,CONSOLA_X,CONSOLA_Y + (w*TAM_FUENTE),255,255,255,SDLApp);
		}
	}
};

struct stSDL{

	SDL_Window * ventana;
	SDL_Renderer * render;

	SDL_Texture * tileset;
	SDL_Texture * personaje;

	TTF_Font * fuente;
	SDL_Texture * textura_fuente;
	SDL_Color colorTexto;
	SDL_Rect rectTexto;
	struct stConsola consola;
};

void iniciarJuego();

void imprimirSDL(struct stPj * pj, char escena[ESCENA_W][ESCENA_H], struct stSDL * SDLApp);
void update(struct stPj * pj, char escnea[ESCENA_W][ESCENA_H]);

void initSDL(struct stSDL * SDLApp);
void inputSDL(struct stPj * pj, char escena[ESCENA_W][ESCENA_H],struct stSDL * SDLApp);
void ponerTexto(char texto[TTFTEXT], int x, int y, int r, int g, int b, struct stSDL * SDLApp);



int BanGo = 1;//eliminar variables globales
int turno = 0;
int main(int argc,char *argv[])
{
	
	int BanConfig = 0;
	if (argc < 1)
	{
		BanConfig = 0;
	}
	else
	{
		BanConfig = 1;
	}
	iniciarJuego();
	
	return 0;
}

void initSDL(struct stSDL * SDLApp)
{
	//Iniciar SDL
	if(SDL_Init(SDL_INIT_EVERYTHING) != 0)
	{
		printf("No se pudo inicializar SDL.\n");
	}

	 SDLApp->ventana = SDL_CreateWindow("SdlApp",SDL_WINDOWPOS_UNDEFINED,SDL_WINDOWPOS_UNDEFINED,PANTALLA_W,PANTALLA_H,SDL_WINDOW_SHOWN);
	
	SDLApp->render = SDL_CreateRenderer(SDLApp->ventana, -1, SDL_RENDERER_ACCELERATED);

	//Iniciar Carga
	SDLApp->tileset = IMG_LoadTexture(SDLApp->render, TILESET_PATH);
	if(SDLApp->tileset == NULL)
	{
		printf("No se pudo cargar la textura: %s \n.",TILESET_PATH);
		SDL_Quit();
	}
	SDLApp->personaje = IMG_LoadTexture(SDLApp->render,TILESET_PATH);
	if(SDLApp->personaje == NULL)
	{
		printf("No se pudo cargar la textura: %s \n.",TILESET_PATH);
		SDL_Quit();
	}

	//Iniciar TTF
	TTF_Init();
	SDLApp->fuente = TTF_OpenFont("Assets/fuente.ttf",TAM_FUENTE);
	if ( SDLApp->fuente == NULL)
	{
		printf("No se pudo cargar la fuente. \n");
		SDL_Quit();
	}

}

void ponerTexto(char texto[TTFTEXT], int x, int y, int r, int g, int b, struct stSDL * SDLApp)
{
	SDLApp->colorTexto.r = r;
	SDLApp->colorTexto.g = g;
	SDLApp->colorTexto.b = b;

	SDLApp->rectTexto.x = x;
	SDLApp->rectTexto.y = y;

	SDLApp->textura_fuente = SDL_CreateTextureFromSurface(SDLApp->render,TTF_RenderText_Solid(SDLApp->fuente,texto,SDLApp->colorTexto));
	
	SDL_QueryTexture(SDLApp->textura_fuente,NULL,NULL,&SDLApp->rectTexto.w,&SDLApp->rectTexto.h);

	SDL_RenderCopy(SDLApp->render,SDLApp->textura_fuente,NULL,&SDLApp->rectTexto);
	SDL_DestroyTexture(SDLApp->textura_fuente);
}

void imprimirSDL(struct stPj * pj, char escena[ESCENA_W][ESCENA_H],struct stSDL * SDLApp)
{
	SDL_RenderClear(SDLApp->render);
	int i, j;
	SDL_Rect rectSrc, rectDST;
	rectSrc.w = rectSrc.h = rectDST.w = rectDST.h = TILES;
	rectSrc.x = rectSrc.y = 0;
	for( i = 0; i < ESCENA_H; i++)
	{
		for(j = 0; j < ESCENA_W; j++)
		{
			rectDST.x = j*TILES;
			rectDST.y = i*TILES;
			
			if(escena[j][i] == PARED_SIM)
			{
				rectSrc.x = PARED_X;
				rectSrc.y = PARED_Y;
				SDL_RenderCopy(SDLApp->render,SDLApp->tileset,&rectSrc,&rectDST);
			}
			else if (escena[j][i] == SUELO_SIM)
			{
				rectSrc.x = SUELO_X;
				rectSrc.y = SUELO_Y;
				SDL_RenderCopy(SDLApp->render,SDLApp->tileset,&rectSrc,&rectDST);
			}
			if(pj->x == j && pj->y == i)
			{
				rectSrc.y = 0;
				rectSrc.x = PERSONAJE_COL*TILES;
				SDL_RenderCopy(SDLApp->render,SDLApp->personaje,&rectSrc,&rectDST);
			}
			

		}
	}
	char txt[TTFTEXT];
	sprintf(txt,"Movimiento: %.2f/%.2f.",pj->movimiento,pj->movimientoMax);
	ponerTexto(txt,CONSOLA_X+PANTALLA_W/2,CONSOLA_Y,255,255,255,SDLApp);

	SDLApp->consola.dibujarConsola(SDLApp);
	SDL_RenderPresent(SDLApp->render);
	//SDL_RenderCopy(SDLApp.render,SDLApp.textura, &rectSRC, &rectDST);

}
void iniciarJuego()
{
	struct stPj pj;
	pj.x = ESCENA_W/2;
	pj.y = ESCENA_H/2;
	pj.vida = PJ_VIDA;
	pj.movimiento = PJ_MOVIMIENTO;
	pj.simbolo = PJ_SIMBOLO;
	pj.movimientoMax = pj.movimiento;
	pj.vidaMax = pj.vida;
	char opc;
	//int BanGo = 1;
	int i, j;
	char escena[ESCENA_W][ESCENA_H];
	
	struct stSDL SDLApp;
	SDLApp.consola.indiceMensajes = 0;
	//Inicializamos la struct SDL
	initSDL(&SDLApp);

	//empezamos seteando la matriz
	
	for(j = 0;j<ESCENA_H;j++)
	{
		for(i=0;i<ESCENA_W;i++)
		{
			if(i == 0 || i == ESCENA_W-1 || j == 0 || j == ESCENA_H-1) 	escena[i][j] = PARED_SIM;
			else escena[i][j] = SUELO_SIM;
		}
	}
	while(BanGo)
	{
		imprimirSDL(&pj,escena,&SDLApp);
		inputSDL(&pj,escena,&SDLApp);
		update(&pj,escena);
	}

}

void inputSDL(struct stPj * pj,char escena[ESCENA_W][ESCENA_H],struct stSDL * SDLApp)
{
	SDL_Event  event;
	char txt[CONSOLA_LEN];
	while(SDL_PollEvent(&event))
	{
		switch(event.type)
		{
		case SDL_QUIT:
				SDL_Quit();
				BanGo = 0; // sacarla de global :/
				break;
		case SDL_KEYDOWN:
			switch(event.key.keysym.sym)
			{
				case SDLK_w:
				if(escena[pj->x][pj->y-1] != '#') pj->y--;
				pj->movimiento--;
				break;
			case SDLK_a:
				if(escena[pj->x-1][pj->y] != '#') pj->x--;
				pj->movimiento--;
				break;
			case SDLK_s:
				if(escena[pj->x][pj->y+1] != '#') pj->y++;
				pj->movimiento--;
				break;
			case SDLK_d:
				if(escena[pj->x+1][pj->y] != '#') pj->x++;
				pj->movimiento--;
				break;
			case SDLK_SPACE:
				pj->movimiento--;
				break;
			case SDLK_y:
				sprintf(txt,"Gritas con todas tus fuerzas!");
				SDLApp->consola.escribirConsola(txt);
				break;
			case SDLK_k:
				sprintf(txt,"Lanzas una patada!");
				SDLApp->consola.escribirConsola(txt);
				break;
			case SDLK_ESCAPE:
				SDL_Quit();
				BanGo = 0;
				break;
			}
			break;
		default:
			break;
		}
	}

}
void update(struct stPj * pj, char escnea[ESCENA_W][ESCENA_H])
{	
	//Restaruamos movimiento del peresonaje.
	if(pj->movimiento < 0)
	{
		pj->movimiento = pj->movimientoMax;
		turno++;
	}
	
}

#include "Objeto.h"


std::string Objeto::getNombre(){ return nombre;};
std::string Objeto::getDesc(){ return desc;};
float Objeto::getPeso(){ return peso;};
//ARMA
Arma::Arma(std::string sNombre,
	std::string sDesc,
	float sPeso,
	float sDmgMin,
	float sDmgMax,
	tipoDmg sClase
	)
{
	nombre = sNombre;
	desc = sDesc;
	peso = sPeso;
	tipo = ARMA;
	dmgMin = sDmgMin;
	dmgMax = sDmgMax;
	dmgClase = sClase;
};
Arma::~Arma(){};

float Arma::getDmgMin(){ return dmgMin;};
float Arma::getDmgMax(){ return dmgMax;};
tipoDmg Arma::getDmgClase(){ return dmgClase; };



//ESCUDO
Escudo::Escudo(std::string sNombre, std::string sDesc, float sPeso, float sProtMin, float sProtMax)
{
	nombre = sNombre;
	desc = sDesc,
	peso = sPeso;
	tipo = ESCUDO;
	protMin = sProtMin;
	protMax = sProtMax;
};
Escudo::~Escudo(){};
float Escudo::getProtMin(){ return protMin; };
float Escudo::getProtMax(){ return protMax; };

//CONSUMIBLE

Consumible::Consumible(std::string sNombre, std::string sDesc, float sPeso, EfectosConsum sEfecto)
{
	nombre = sNombre;
	desc = sDesc;
	peso = sPeso;
	tipo = CONSUMIBLE;
	efecto = sEfecto;
};

Consumible::~Consumible(){};

void Consumible::usar()
{
	//TODO
};

//VESTIMENTA

Vestimenta::Vestimenta(std::string sNombre, std::string sDesc, float sPeso,float sProtMin, float sProtMax, vestTipo sVestClase)
{
	nombre = sNombre;
	desc = sDesc;
	peso = sPeso;
	tipo = VESTIMENTA;
	protMin = sProtMin;
	protMax = sProtMax;
	vestClase = sVestClase;
};
Vestimenta::~Vestimenta(){};

float Vestimenta::getProtMin(){ return protMin;};
float Vestimenta::getProtMax(){ return protMax;};

//COMIDA

Comida::Comida(std::string sNombre, std::string sDesc, float sPeso,float sPuntos)
{
	nombre = sNombre;
	desc = sDesc;
	peso = sPeso;
	tipo = COMIDA;
	puntos = sPuntos;
};

Comida::~Comida(){};

void Comida::consumir()
{
	//TODO
};

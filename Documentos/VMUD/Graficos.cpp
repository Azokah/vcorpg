#include "Graficos.h"

Graficos::Graficos()
{
	inputCMD = false;
	if(SDL_Init(SDL_INIT_EVERYTHING) != 0 ) getError("No se pudo inicializar SDL.");
	ventana = SDL_CreateWindow(TITULO_VENTANA, SDL_WINDOWPOS_UNDEFINED,SDL_WINDOWPOS_UNDEFINED,PANTALLA_AL,PANTALLA_AN,SDL_WINDOW_SHOWN);
	render = SDL_CreateRenderer(ventana,-1,SDL_RENDERER_ACCELERATED);

	tileset = cargarTextura(render,ASSETS_PATH);
	
	rectDestino.x = rectDestino.y = rectZona.x = rectZona.y = 0;
	rectDestino.h = TILE_AL;
	rectDestino.w = TILE_AN;
	rectZona.h = rectZona.w = TILE_AN;
};

Graficos::~Graficos(){};

SDL_Texture * Graficos::cargarTextura(SDL_Renderer * renderer, std::string path)
{
	SDL_Texture * textura;
	textura = IMG_LoadTexture(renderer,path.c_str());
	if( textura == NULL)
	{
		getError("No se pudo cargar la textura:" + path);
		return nullptr;
	}
	return textura;

};
void Graficos::limpiarRender(){ SDL_RenderClear(render);};
void Graficos::renderizar(){SDL_RenderPresent(render);};
void Graficos::update(Mapa * mapa, int x, int y)
{
	if(mapa->getMapa(x,y) == 1)
	{
		rectZona.x = TILE_PAREDX*TILE_AN;
		rectZona.y = TILE_PAREDY*TILE_AL;
	} else {
		rectZona.x = TILE_ROCAX*TILE_AN;
		rectZona.y = TILE_ROCAY*TILE_AL;
	};
	rectDestino.x = x * TILE_AN;
	rectDestino.y = y * TILE_AL;
	//dibujo va aca, funcion?, funcion?
	SDL_RenderCopy(render,tileset,&rectZona,&rectDestino);
};

void Graficos::input(Comandos * comando)
{
	SDL_Event  event;
	while(SDL_PollEvent(&event))
	{
		switch(event.type)
		{
		case SDL_QUIT:
				SDL_Quit();
			break;
		case SDL_KEYDOWN:
			if(event.key.keysym.sym == 13 && !inputCMD)
			{
				std::cout<<"Empieza input."<<std::endl;
				inputCMD = true;
				cmd = "";
			}
			else if ( event.key.keysym.sym == 13 && inputCMD)
			{
				std::cout<<"Termina INPUT: "<<cmd.c_str()<<std::endl;
				inputCMD = false;
				comando->cmd(cmd);
				cmd = "";
			}
			if(inputCMD && event.key.keysym.sym == 8)
			{
				if(cmd.length() > 0) cmd.erase (cmd.end()-1);
			}
			else if(inputCMD && event.key.keysym.sym != 13 && event.key.keysym.sym != 8)
			{
				cmd = cmd + static_cast<char>(event.key.keysym.sym);
			}
			switch(event.key.keysym.sym)
			{
			case SDLK_ESCAPE:
				SDL_Quit();	
				break;
			}
			break;
		default:
			break;
		}
	}
}

#pragma once
#include <iostream>
#include <string>


class EfectosConsum
{
	//Interfaz de efectos para consumibles
	public:
		virtual ~EfectosConsum(){};
		
		void activar();
		std::string getNombre();
		std::string getDesc();
	protected:
		std::string nombre;
		std::string desc;
};

class ECuracion : public EfectosConsum
{
	//Curacion instantanea
	public:
		ECuracion(std::string sNombre, std::string sDesc, float sPuntos);
		~ECuracion();

		void activar();

	private:
		float puntos;
};

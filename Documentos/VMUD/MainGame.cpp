#include "MainGame.h"

// Hacer vector de comandos y empezar el comandeo lord commander comandante 
// TODO:
// - Crear metodos para cargar comandos de archivo
// - Crear metodos para cargar objetos de archivo
// - Struct de bindeo ( String + Comando)
// - Golpe de personaje
// - Agregar SDL
// - Mas comandos
// - Movimiento de personaje con flechas


MainGame::MainGame()
{
	gameOn = true;
	nuevoTurno = false;
	mapa = new Mapa();
	pj = new Personaje();
	graficos = new Graficos();
	comandos = new Comandos(pj,mapa,&gameOn);
	timer = new Timer();
	timer->start();
};

MainGame::~MainGame(){};

void MainGame::init()
{
	proximoTick = timer->get_ticks() + TIEMPO_TURNOS;
	while(gameOn){
		while(!nuevoTurno)
		{
			graficos->limpiarRender();
			for(int i = 0; i < MAPA_H; i++)
			{
				
				for(int j = 0; j < MAPA_W; j++)
				{
					graficos->update(mapa,j,i);
				}
			}
			graficos->renderizar();
			graficos->input(comandos);
			pasarTurno();
		}
		nuevoTurno = false;
	}
};

void MainGame::pasarTurno()
{
	
	if(timer->get_ticks() >= proximoTick)
	{
		proximoTick = timer->get_ticks() + TIEMPO_TURNOS;
		nuevoTurno = true;
		std::cout<<"Ticks:" << timer->get_ticks() << ". Prox Tick: "<<proximoTick<<std::endl;
	}
	else
	{
		nuevoTurno = false;
	}
};

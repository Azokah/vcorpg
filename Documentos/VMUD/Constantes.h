//Constantes de juego
#define TIEMPO_TURNOS 4000

//

//Constantes SDL
#define PANTALLA_AL 800
#define PANTALLA_AN 600
#define TITULO_VENTANA "VMUD"
#define ASSETS_PATH "Assets/tileset.png"
#define TILE_AL 16
#define TILE_AN 16


//Constantes Tileset
#define TILE_PAREDY 0
#define TILE_PAREDX 0
#define TILE_ROCAX 7
#define TILE_ROCAY 0


// Constantes del mapa
#define MAPA_W 50
#define MAPA_H 50
#define SUELO 1
#define PARED 0
#define PUERTA 3



//Constantes del personaje
#define PJ_NAME 25
#define PJ_DESC 50

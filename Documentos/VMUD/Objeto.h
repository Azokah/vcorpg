#pragma once
#include <iostream>
#include <string>
#include "Constantes.h"
#include "EfectosConsum.h"

enum tipoObjeto
{
	ARMA,
	ESCUDO,
	VESTIMENTA,
	CONSUMIBLE,
	COMIDA
};

class Objeto
{
	public:	
		virtual ~Objeto(){};
		std::string getNombre();
		std::string getDesc();
		float getPeso();

		
	protected:
		
		std::string nombre;
		std::string desc;

		float peso;

		tipoObjeto tipo;
};

enum tipoDmg
{
	CORTANTE,
	APLASTANTE,
	PERFORANTE,
	MAGICO
};

class Arma : public Objeto
{
	public:
		Arma(std::string sNombre,
				std::string sDesc,
				float sPeso,
				float sDmgMin,
				float sDmgMax,
				tipoDmg sClase);
		~Arma();
		
		

		float getDmgMin();
		float getDmgMax();

		tipoDmg getDmgClase();

	protected:

		float dmgMin, dmgMax;
		tipoDmg dmgClase;
};

class Escudo : public Objeto
{
	public:
		Escudo(std::string sNombre, std::string sDesc, float sPeso, float sProtMin, float sProtMax);
		~Escudo();
		
		float getProtMin();
		float getProtMax();

	private:
		float protMin,protMax;
};


class Consumible : public Objeto
{
	public:
		Consumible(std::string sNombre, std::string sDesc, float sPeso,EfectosConsum sEfecto);
		~Consumible();
		
		void usar();

	private:
		EfectosConsum efecto;		
};
enum vestTipo
{
	TELA,
	CUERO,
	CUERO_ENDURECIDO,
	CUERO_TACHONADO,
	MALLAS,
	PLACAS
};
class Vestimenta : public Objeto
{
	public:
		Vestimenta(std::string sNombre, std::string sDesc, float sPeso, float sProtMin, float sProtMax, vestTipo sVestClase);
		~Vestimenta();
		
		float getProtMin();
		float getProtMax();

	private:
		float protMin, protMax;
		vestTipo vestClase;
};

class Comida : public Objeto
{
	public:
		Comida(std::string sNombre, std::string sDesc, float sPeso,float sPuntos);
		~Comida();

		void consumir();

	private:
		float puntos;
};

#include "Mapa.h"


Mapa::Mapa()
{
	numero = 0;
	resetMapa();
	objMapa  obj;
	obj.objeto = new Arma("Espada","Legendaria espada de las mil verdades.",32,50,100,CORTANTE);
	obj.x = 5;
	obj.y = 5;
	objetos.push_back(obj);
	obj.objeto = new Arma("Espada","Espada de madera.",0,1,5,CORTANTE);
	obj.x = 10;
	objetos.push_back(obj);
};

Mapa::~Mapa(){};

void Mapa::resetMapa()
{
	for(int i = 0;i < MAPA_H; i++)
	{
		for(int j = 0;j < MAPA_W; j++)
		{
			mapa[i][j] = 0;
			if( i == 0 || i == MAPA_H -1 || j == 0 || j == MAPA_W -1) mapa[i][j] = 1;
		}

	}
};

int Mapa::getMapa(int x, int y)
{
	return mapa[y][x];
};


std::deque<objMapa>* Mapa::getObjetos(){ return &objetos; };

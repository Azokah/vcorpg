#pragma once
#include <iostream>
#include <deque>
#include "Constantes.h"
#include <stdlib.h>
#include <time.h>
class Mapa
{
	public:
		Mapa();
		~Mapa();
		int getMapa(int x, int y);
		void resetMapa();
	private:
		int numero;
		int mapa[MAPA_H][MAPA_W];
			
	
};

#include "InterAcciones.h"

InterAcciones::InterAcciones(bool * GAMEON,Camara * CAMARA, Mapa * MAPA){
	gameOn = GAMEON;
	camara = CAMARA;
	mapa = MAPA;
	quitOn =  camaraNorte = camaraSur = camaraOeste = camaraEste = false;
};
InterAcciones::~InterAcciones(){};

void InterAcciones::run(){
	if(camaraNorte) camara->setCamaraXY(camara->getX(),camara->getY()-VELOCIDAD_CAMARA);
	if(camaraSur) camara->setCamaraXY(camara->getX(),camara->getY()+VELOCIDAD_CAMARA);
	if(camaraOeste) camara->setCamaraXY(camara->getX()-VELOCIDAD_CAMARA,camara->getY());
	if(camaraEste) camara->setCamaraXY(camara->getX()+VELOCIDAD_CAMARA,camara->getY());
	if(quitOn) *gameOn = false;
};
void InterAcciones::moverCamaraNorte(bool x){camaraNorte = x;};
void InterAcciones::moverCamaraSur(bool x){camaraSur = x;};
void InterAcciones::moverCamaraEste(bool x){camaraEste = x;};
void InterAcciones::moverCamaraOeste(bool x){camaraOeste = x;};

void InterAcciones::quit(){quitOn = true;};

void InterAcciones::resetearMapa(){ mapa->resetMapa();};


#include "MainGame.h"

/*
	Hago esto antes de olvidarme mi idea.
	La idea es hacer una arena donde personajes con distinto equipo
	y caracteristicas pelean mediante un comportamiento programado en
	un determinado script. Con las reglas y balance del Pathfinder.



*/
MainGame::MainGame()
{
	gameOn = true;
	FPS = FPSMAX = 0;
	mapa = new Mapa();
	sdl = new SdlHelper();
	timer = new Timer();
	timer->start();
	proximoTick = timer->get_ticks() + 1000;
	camara = new Camara();
	interfaz = new InterAcciones(&gameOn,camara,mapa);
};

MainGame::~MainGame(){};

void MainGame::init()
{
	while(gameOn){
		sdl->input(interfaz);
		for(int i = 0; i < MAPA_H; i++)
		{
			for(int j = 0; j < MAPA_W; j++)
			{
				sdl->update(mapa, j, i, camara,interfaz);
			}
		}
		interfaz->run();
		drawFPS();
		sdl->renderizar();
		sdl->limpiarRender();
		FPS++;
		
	}


};

void MainGame::drawFPS(){
	if(timer->get_ticks() >= proximoTick) {
		proximoTick = timer->get_ticks() + 1000;
		FPSMAX = FPS;
		FPS = 0;
	}
	char txt[25];
	sprintf(txt,"FPS: %d",FPSMAX);
	sdl->imprimirTexto(txt,25,50,255,0,125);

};

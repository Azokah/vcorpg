#include "Mapa.h"


Mapa::Mapa()
{
	srand(time(NULL));
	numero = 0;

	resetMapa();
};

Mapa::~Mapa(){};

void Mapa::resetMapa()
{
	for(int i = 0;i < MAPA_H; i++)
	{
		for(int j = 0;j < MAPA_W; j++)
		{
			mapa[i][j] = 0;
			//Para agregar arboles aleatorios re down
			numero = rand()%60 + 1;
			if((i+j)%numero == 1) mapa[i][j] = 2;
			//Fin arboles aleatorioes
			if( i == 0 || i == MAPA_H -1 || j == 0 || j == MAPA_W -1) mapa[i][j] = 1;
			
		}

	}
};

int Mapa::getMapa(int x, int y)
{
	return mapa[y][x];
};



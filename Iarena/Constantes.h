//Constantes de juego


//Constantes SDL
#define PANTALLA_AL 600
#define PANTALLA_AN 800
#define TITULO_VENTANA "Proyecto"
#define ASSETS_PATH "Assets/Tileset.png"
#define TILE_AL 64
#define TILE_AN 64
#define HUD_PATH "Assets/HUD.png"

//Constantes Camara
#define CAMARA_AN PANTALLA_AN
#define CAMARA_AL PANTALLA_AL
#define VELOCIDAD_CAMARA 24
#define BORDE_CAMARA 25


//Constantes Tileset
#define TILE_PAREDY 0
#define TILE_PAREDX 1
#define TILE_ROCAX 0
#define TILE_ROCAY 0
#define TILE_PJX 0
#define TILE_PJY 0
#define TILE_OBJETOX 0
#define TILE_OBJETOY 0

// Constantes del mapa
#define MAPA_W 50
#define MAPA_H 50
#define SUELO 1
#define PARED 0



//Constantes del personaje
#define PJ_NAME 25

//Constantes de la Fuente
#define TEXTO_SIZE 12
#define TEXTO_ALPHA 0
#define TEXTO_FUENTE "Assets/ARCADE_N.TTF"

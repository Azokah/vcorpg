#pragma once
#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>


class Hud {

	public:
		Hud();
		~Hud();
	
		void setTexture(SDL_Texture * texture);
		SDL_Texture * getTexture();
	
		int getX();
		int getY();
	private:
	
		SDL_Texture * hudTexture;
		int x, y;


};

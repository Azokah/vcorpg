#include "Camara.h"


Camara::Camara(){
	x = 0;
	y = 0;
	w = CAMARA_AN;
	h = CAMARA_AL;
};

Camara::~Camara(){};

int Camara::getX(){ return x;};

int Camara::getY(){ return y;};

int Camara::getW(){ return w;};

int Camara::getH(){ return h;};


void Camara::setCamaraXY(int nx, int ny) {
	
       	 x = nx;
	 y = ny;

	 if(x > ((TILE_AN*MAPA_W) - w)){
	 	x = ((TILE_AN*MAPA_W)-w);
	 } else if (x < 0) {
		x = 0;
	 }

	 if (y < 0 ) {
		y = 0;
	 } else if (y > ((TILE_AL*MAPA_H) - h)) {
		y = (TILE_AL*MAPA_H) - h;
	 }

};

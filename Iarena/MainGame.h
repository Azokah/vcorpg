#pragma once
#include <iostream>
#include <string.h>
#include <deque>
#include "Constantes.h"
#include "Mapa.h"
#include "SdlHelper.h"
#include "Timer.h"
#include "Camara.h"
#include "InterAcciones.h"

class MainGame {
	public:
		MainGame();
		~MainGame();
		void init();
	private:
		
		void pasarTurno();
		int proximoTick;
		bool gameOn;
		void drawFPS();

		int FPS,FPSMAX;
		//Clases
		Mapa * mapa;
		SdlHelper * sdl;
		Timer * timer;
		Camara * camara;			
		InterAcciones * interfaz;
};	

#include "SdlHelper.h"

SdlHelper::SdlHelper()
{
	debugOn = false;
	if(SDL_Init(SDL_INIT_EVERYTHING) != 0 ) getError("No se pudo inicializar SDL.");
	ventana = SDL_CreateWindow(TITULO_VENTANA, SDL_WINDOWPOS_UNDEFINED,SDL_WINDOWPOS_UNDEFINED,PANTALLA_AN,PANTALLA_AL,SDL_WINDOW_SHOWN);
	render = SDL_CreateRenderer(ventana,-1,SDL_RENDERER_ACCELERATED|SDL_RENDERER_PRESENTVSYNC);

	tileset = cargarTextura(render,ASSETS_PATH);

	rectDestino.x = rectDestino.y = rectZona.x = rectZona.y = 0;
	rectDestino.h = TILE_AL;
	rectDestino.w = TILE_AN;
	rectZona.h = rectZona.w = TILE_AN;

	texto = new Texto();
	texto->init(TEXTO_SIZE);
	texto->setAlpha(TEXTO_ALPHA);
	hud = new Hud();
	hud->setTexture(cargarTextura(render,HUD_PATH));
};
SdlHelper::~SdlHelper(){};

void SdlHelper::imprimirTexto(std::string mensaje, int x, int y, int r, int g, int b)
{

	texto->ponerTexto(mensaje,x,y,r,g,b,render);

}

SDL_Texture * SdlHelper::cargarTextura(SDL_Renderer * renderer, std::string path)
{
	SDL_Texture * textura;
	textura = IMG_LoadTexture(renderer,path.c_str());
	if( textura == NULL)
	{
		getError("No se pudo cargar la textura:" + path);
		return nullptr;
	}
	return textura;

};
void SdlHelper::limpiarRender(){ SDL_RenderClear(render);};
void SdlHelper::renderizar(){
	SDL_RenderCopy(render,hud->getTexture(),NULL,NULL);
	SDL_RenderPresent(render);
};
void SdlHelper::update(Mapa * mapa, int y, int x, Camara * camara,InterAcciones * interfaz)
{
	//Dibujo dentro de los confines de la camara
	if(x*TILE_AN >= camara->getX()-TILE_AN && x*TILE_AN <= camara->getX()+camara->getW()){
		if(y*TILE_AL >= camara->getY()-TILE_AL && y*TILE_AL <= camara->getY()+camara->getH()){
			if(mapa->getMapa(x,y) == 1){
				rectZona.x = TILE_PAREDX*TILE_AN;
				rectZona.y = TILE_PAREDY*TILE_AL;
			} else if(mapa->getMapa(x,y) == 2){
				rectZona.x = 0*TILE_AN;
				rectZona.y = 1*TILE_AL;	
			}else {
				rectZona.x = TILE_ROCAX*TILE_AN;
				rectZona.y = TILE_ROCAY*TILE_AL;
			}
			rectDestino.x =  (x * TILE_AN ) - camara->getX();
			rectDestino.y =  (y * TILE_AL ) - camara->getY();
			
			SDL_RenderCopy(render,tileset,&rectZona,&rectDestino);

			//Aca abajo deberia hacerse en funcion aparte Dibujar elementos HUD
			//dibujo va aca
			//Tambien estoy dibujando textos desde el MAIN y deben ser parte del HUD
			char txt[10];
			sprintf(txt,"%d-%d",x,y);
			if(debugOn)imprimirTexto(txt,rectDestino.x,rectDestino.y,255,125,0);
			
		}	
	}

}

void SdlHelper::input(InterAcciones * interfaz)
{
	moverCamara(interfaz);
	
	SDL_Event  event;
	while(SDL_PollEvent(&event))
	{
		switch(event.type)
		{
			case SDL_KEYDOWN:

				switch( event.key.keysym.sym ){

					case SDLK_ESCAPE:
						interfaz->quit();	
						break;
					case SDLK_UP:
						interfaz->moverCamaraNorte(true);
						break;
					case SDLK_DOWN:
						interfaz->moverCamaraSur(true);
						break;
					case SDLK_LEFT:
						interfaz->moverCamaraOeste(true);
						break;
					case SDLK_RIGHT:
						interfaz->moverCamaraEste(true);
						break;
					case SDLK_F1:
						if(debugOn) debugOn = false;
						else debugOn = true;
						break;
					case SDLK_F2:
						interfaz->resetearMapa();
						break;
					default:
						break;
				}
				break;
			case SDL_QUIT:
				SDL_Quit();
				interfaz->quit();
				break;
			case SDL_MOUSEBUTTONDOWN:
				switch(event.button.button)
				{
					case SDL_BUTTON_LEFT:
						break;
					case SDL_BUTTON_RIGHT:
						break;
					default:
						break;
				}
				break;
		}
	}
}

void SdlHelper::moverCamara(InterAcciones * interfaz){
	int  mousex, mousey;
	SDL_GetMouseState(&mousex, &mousey);
	interfaz->moverCamaraOeste(false);
	interfaz->moverCamaraEste(false);
	interfaz->moverCamaraNorte(false);
	interfaz->moverCamaraSur(false);
	if(mousex < BORDE_CAMARA) interfaz->moverCamaraOeste(true);
	if(mousex > CAMARA_AN-BORDE_CAMARA) interfaz->moverCamaraEste(true);
	if(mousey < BORDE_CAMARA) interfaz->moverCamaraNorte(true);
	if(mousey > CAMARA_AL-BORDE_CAMARA) interfaz->moverCamaraSur(true);


};



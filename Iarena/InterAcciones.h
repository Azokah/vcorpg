#pragma once
#include <iostream>
#include "Constantes.h"
#include "Camara.h"
#include "Mapa.h"


class InterAcciones {
	public:
		InterAcciones(bool * GAMEON,Camara * CAMARA,Mapa * MAPA);
		~InterAcciones();

		void run();
		void moverCamaraNorte(bool);
		void moverCamaraSur(bool);
		void moverCamaraEste(bool);
		void moverCamaraOeste(bool);
	
		void quit();

		void resetearMapa();
	
	private:
	
		bool  quitOn, camaraNorte, camaraSur, camaraEste, camaraOeste;
		Camara * camara;
		bool * gameOn;
		Mapa * mapa;
};


#pragma once
#include <iostream>
#include <deque>
#include "Constantes.h"
#include "Objeto.h"

struct objMapa
{
	Objeto * objeto;
	int x, y;
};
class Mapa
{
	public:
		Mapa();
		~Mapa();
		int getMapa(int x, int y);
		std::deque<objMapa> *  getObjetos();
	private:
		int numero;
		int mapa[MAPA_H][MAPA_W];
		void resetMapa();
		std::deque<objMapa> objetos;	
	
};

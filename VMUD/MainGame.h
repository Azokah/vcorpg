#pragma once
#include <iostream>
#include <string.h>
#include <deque>
#include "Constantes.h"
#include "Mapa.h"
#include "Personaje.h"
#include "Objeto.h"
#include "Comandos.h"
#include "Graficos.h"
#include "Timer.h"

class MainGame {
	public:
		MainGame();
		~MainGame();
		void init();
	private:
		
		void pasarTurno();
		int proximoTick;
		bool nuevoTurno;
		bool gameOn;

		//Clases
		Mapa * mapa;
		Personaje * pj;
		Comandos * comandos;
		Graficos * graficos;
		Timer * timer;
		

};	

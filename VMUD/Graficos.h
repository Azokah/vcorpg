#pragma once
#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include "Constantes.h"
#include <string.h>
#include "Mapa.h"
#include "getError.h"
#include "Comandos.h"

class Graficos {

	public:
		Graficos();
		~Graficos();
		
		void update(Mapa * mapa, int x, int y);
		void limpiarRender();
		void renderizar();

		void input(Comandos * comando);
	private:
		bool inputCMD;
		std::string cmd;
		SDL_Texture * cargarTextura(SDL_Renderer * renderer, std::string path);

		SDL_Window * ventana;
		SDL_Renderer * render;

		SDL_Texture * tileset;
		SDL_Rect  rectDestino,  rectZona;

};

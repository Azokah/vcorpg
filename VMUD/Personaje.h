#pragma once
#include <iostream>
#include <string>
#include <deque>
#include "Constantes.h"
#include "Objeto.h"

class Personaje {
	public:
		Personaje();
		~Personaje();
		
		void setStats(float svida, float svidaMax, float sspellPoints, float sspellPointsMax, float sstamina, float sstaminaMax, float sAC, float sBMC, float sDMC, float saFu, float saAgi, float saInt, float saCar, float saSab, std::string sname, std::string sdesc);
		
		void setVida(float sVida);

		std::string getNombre();
		float getVida();
		float getVidaMax();
		float getSpellPoints();
		float getSpellPointsMax();
		float getStamina();
		float getStaminaMax();
		float getAC();
		float getBMC();
		float getDMC();
		float getAFu();
		float getAAgi();
		float getAInt();
		float getACar();
		float getASab();

		void setPos(int sX, int sY);
		void moveTo(int sX, int sY);
		
		int getX();
		int getY();

		void move();
		
		void mirar(std::deque<Objeto> item);
		void agarrar(Objeto * item, int x, int y);
	private:
		float vida, vidaMax;
		float spellPoints, spellPointsMax;
		float stamina, staminaMax;
		float AC, BMC, DMC;
		float aFu, aAgi,aInt,aCar,aSab;

		std::string nombre;
		std::string desc;

		int x,y;
		int dX, dY;

		std::deque<Objeto*> inventario; 
};

/* * * * * * * * * * * * * * * * * * *
 * By Jaime Gonzalez Bonorino  * * * *
 * E-MAIL: jaimegbonorino@gmail.com* *
 * Project: Visual MUD RPG * * * * * *
 * License: Freeware, donations* * * *
 * Open Source, feel free to do what *
 * you want with this code * * * * * *
 * * * * * * * * * * * * * * * * * * */

#include <iostream>
#include "MainGame.h"

using namespace std;

int main(int argc, char * argv[])
{
	MainGame game;
	game.init();
	return 0;
}


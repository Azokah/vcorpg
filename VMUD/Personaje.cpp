#include "Personaje.h"

Personaje::Personaje()
{
	vida = 100;
	vidaMax = 100;
	spellPoints = 0;
	spellPointsMax = 0;
	stamina = 100;
	staminaMax = 100;
	AC = BMC = DMC = aFu = aAgi = aInt = aCar = aSab = 0;
	nombre = "Fulanito";
	desc = "Jugador regular";
	x = 1;
	dX = 1;
	y = 1;
	dY = 1;
};
Personaje::~Personaje(){};

void Personaje::setStats(
		float svida, float svidaMax,
		float sspellPoints, float sspellPointsMax,
		float sstamina, float sstaminaMax,
		float sAC, float sBMC, float sDMC,
		float saFu, float saAgi, float saInt, float saCar, float saSab,
		std::string sname, std::string sdesc)
{
	vida = svida;
	vidaMax = svidaMax;
	spellPoints = sspellPoints;
	spellPointsMax = sspellPointsMax;
	stamina = sstamina;
	staminaMax = sstaminaMax;
	AC = sAC;
	BMC = sBMC;
	DMC = sDMC;
	aFu = saFu;
	aAgi = saAgi;
	aInt = saInt;
	aCar = saCar;
	aSab = saSab;
	nombre = sname;
	desc = sdesc;
}

std::string Personaje::getNombre()
{
	return nombre + " - " + desc;
}

void Personaje::setVida(float sVida)
{
	vida = sVida;
	if(vida > vidaMax) vida = vidaMax;
}

void Personaje::setPos(int sX, int sY)
{
	x = sX;
	y = sY;
};
void Personaje::moveTo(int sX, int sY)
{
	dX = sX;
	dY = sY;
};

void Personaje::mirar(std::deque<Objeto> item)
{

}
void Personaje::agarrar(Objeto * item,int x, int y)
{
	inventario.push_back(item);
	std::cout << "Se agrego "<< item->getNombre() << " ("<<item->getDesc()<<")"<<std::endl;
}



// getters
int Personaje::getX()
{
	return x;
};
int Personaje::getY()
{
	return y;
};

void Personaje::move()
{
	x = dX;
	y = dY;
};

float Personaje::getVida()
{
	return vida;
}
float Personaje::getVidaMax(){
	return vidaMax;
};
float Personaje::getSpellPoints(){
	return spellPoints;
};
float Personaje::getSpellPointsMax(){
	return spellPointsMax;
};
float Personaje::getStamina(){
	return stamina;
};
float Personaje::getStaminaMax(){
	return staminaMax;
};
float Personaje::getAC(){
	return AC;
};
float Personaje::getBMC(){
	return BMC;
};
float Personaje::getDMC(){
	return DMC;
};
float Personaje::getAFu(){
	return aFu;
};
float Personaje::getAAgi(){
	return aAgi;
};
float Personaje::getAInt(){
	return aInt;
};
float Personaje::getACar(){
	return aCar;
};
float Personaje::getASab(){
	return aSab;
};
